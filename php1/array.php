<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>contoh soal array</h1>
    <?php
    $trainer = ["rezky", "yogi", "abdul", "asep"];
    echo $trainer[0];
    print_r($trainer);
    echo "<br>";
    echo "Data trainer <br>";
    echo "Total trainer : " . count($trainer) . "<br>";
    echo "<ol>";
    echo "<li> " . $trainer[0] . "</li>"; 
    echo "<li> " . $trainer[1] . "</li>"; 
    echo "<li> " . $trainer[2] . "</li>"; 
    echo "<li> " . $trainer[3] . "</li>";
    echo "</ol>" ;
    $bioTrainer = [
        ["nama" => "rezky", "kota" => "jakarta", "materi" => "laravel"],
        ["nama" => "yogi", "kota" => "makassar", "materi" => "golang"],
        ["nama" => "abdul", "kota" => "bandung", "materi" => "Reactjs"],
        ["nama" => "asep", "kota" => "garut", "materi" => "wordpress"]
    ];
    echo "<pre>";
    print_r($bioTrainer);
    echo "</pre>";
    ?>
       <h1>Berlatih Array</h1>

<?php
echo "<h3> Soal 1 </h3>";
/* 
        SOAL NO 1
        Kelompokkan nama-nama di bawah ini ke dalam Array.
        Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
        Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
    */
$kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" ]; 
print_r($kids);// Lengkapi di sini
$adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
print_r($adults);
echo "<h3> Soal 2</h3>";
/* 
        SOAL NO 2
        Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
    */
echo "Cast Stranger Things: ";
echo "<br>";
echo "Total Kids: " . count($kids) . "<br>"; // Berapa panjang array kids
echo "<br>";
echo "<ol>";
echo "<li> $kids[0] </li>";
echo "<li> $kids[1] </li>";
echo "<li> $kids[2] </li>";
echo "<li> $kids[3] </li>";
echo "<li> $kids[4] </li>";
echo "<li> $kids[5] </li>";
echo "</ol>";
// Lanjutkan

echo "</ol>";

echo "Total Adults: " . count($adults) . "<br>";; // Berapa panjang array adults
echo "<br>";
echo "<ol>";
echo "<li> $adults[0] </li>";
echo "<li> $adults[1] </li>";
echo "<li> $adults[2] </li>";
echo "<li> $adults[3] </li>";
echo "<li> $adults[4] </li>";
echo "</ol>";
// Lanjutkan

echo "</ol>";
$bioData = [
    ["Name" => "Will Byers", "Age" => 12 , "Aliases" => "Will the Wise" ,"Status" => "Alive"],
    ["Name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
    ["Name" => "Jim Hopper", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
    ["Name" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive"]
];
echo "<pre>";
print_r($bioData);
echo "</pre>";

/*
        SOAL No 3
        Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
        
        Name: "Will Byers"
        Age: 12,
        Aliases: "Will the Wise"
        Status: "Alive"

        Name: "Mike Wheeler"
        Age: 12,
        Aliases: "Dungeon Master"
        Status: "Alive"

        Name: "Jim Hopper"
        Age: 43,
        Aliases: "Chief Hopper"
        Status: "Deceased"

        Name: "Eleven"
        Age: 12,
        Aliases: "El"
        Status: "Alive"


        Output:
        Array
            (
                [0] => Array
                    (
                        [Name] => Will Byers
                        [Age] => 12
                        [Aliases] => Will the Wise
                        [Status] => Alive
                    )

                [1] => Array
                    (
                        [Name] => Mike Wheeler
                        [Age] => 12
                        [Aliases] => Dugeon Master
                        [Status] => Alive
                    )

                [2] => Array
                    (
                        [Name] => Jim Hooper
                        [Age] => 43
                        [Aliases] => Chief Hopper
                        [Status] => Deceased
                    )

                [3] => Array
                    (
                        [Name] => Eleven
                        [Age] => 12
                        [Aliases] => El
                        [Status] => Alive
                    )

            )
        
    */
?>
</body>
</html>