<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>contoh string</h1>
    <?php
    echo"<h3> contoh soal 1</h3>";
    $kalimat1 = "lorem ipsum";
    echo "kalimat pertama : " . $kalimat1 . "<br>";
    echo "panjang kalimat 1 : " . strlen($kalimat1) . "<br>";
    echo "jumlah kata kalimat 1 : " . str_word_count($kalimat1) . "<br>";
    echo"<h3> contoh soal 2</h3>";
    $kalimat2 = "selamat datang di sanbercode";
    echo "kalimat kedua : " . $kalimat2 . "<br>";
    echo "kata 1 kalimat kedua : " . substr($kalimat2,0,7) . "<br>";
    echo "kata 2 kalimat kedua : " . substr($kalimat2,8,6) . "<br>";
    echo "kata 3 kalimat kedua : " . substr($kalimat2,15,2) . "<br>";
    echo "kata 4 kalimat kedua : " . substr($kalimat2,18,10) . "<br>";
    echo"<h3> contoh soal 3</h3>";
    $kalimat3 = "selamat sore";
    echo "kalimat ketiga : " . $kalimat3 . "<br>";
    echo "Ganti string kalimat ke 3 : " . str_replace("sore", "pagi", $kalimat3);
    ?>
     <h1>Berlatih String PHP</h1>
     <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ;
        echo "panjang string : " . strlen($first_sentence) . "<br>";
        echo "jumlah kata : " . str_word_count($first_sentence) . "<br>";
         // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; 
        echo "panjang string : " . strlen($second_sentence) . "<br>";
        echo "jumlah kata : " . str_word_count($second_sentence) . "<br>";
        // Panjang string: 28,  jumlah kata: 5
        
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>" ;
        echo "<br> Kata Ketiga: " . substr($string2, 7, 3) . "<br>" ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" ";
        echo "String: "  . str_replace("sexy", "awesome", $string3);;  
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>