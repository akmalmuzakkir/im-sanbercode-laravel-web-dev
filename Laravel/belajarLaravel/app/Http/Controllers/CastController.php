<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }
    public function store(Request $request){
        $request-> validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],[
            'nama.required' => 'nama harus disi tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'bio.required' => 'bio tidak boleh kosong'
        ]);
        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);
        return redirect('/cast');
    }
    public function index(){
       $casts = DB::table('casts') -> get();
    //    dd($casts);
       return view('cast.tampil',['casts' => $casts]);
    }
    public function show($id){
      $cast = DB::table('casts')->find($id); 
      return view('cast.detail', ['cast' => $cast]);

    }
    public function edit($id){
        $cast = DB::table('casts')->find($id); 
        return view('cast.edit', ['cast' => $cast]); 
    }
    public function update($id, Request $request){
        $request-> validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],[
            'nama.required' => 'nama harus disi tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'bio.required' => 'bio tidak boleh kosong'
        ]);
        DB::table('casts')
              ->where('id', $id)
              ->update(
                [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
            );
        return redirect('/cast');
    }
    public function destroy($id){
        DB::table('casts')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
