<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'dashboard']);
Route::get('/pendaftaran', [AuthController::class,'daftar']);
Route::post('/kirim', [AuthController::class, 'kirim']);
//testing master template
// Route::get('/master', function(){
//     return view('layouts.master');
// });
Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/table', function(){
    return view('page.table');
});
//crud
//create
//route untuk menambah data cast
Route::get('cast/create', [CastController::class,'create']);
//route untuk menyimpan inputan ke dalam database tabel cast
Route::post('/cast', [CastController::class, 'store']);
//read data
//route mengarah ke halaman tampil semua data
Route::get('/cast',[CastController::class , 'index']);
//route detail kategori berdasarkan id
Route::get('/cast/{id}',[CastController::class , 'show']);
//update data
//route mengarah form edit kategori
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//route untuk edit data berdasarkan id cast
Route::put('/cast/{id}', [CastController::class, 'update']);
//delete data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
