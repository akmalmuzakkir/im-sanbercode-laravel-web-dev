<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @extends('layouts.master')
    @section('title')
    Halaman Biodata
        
    @endsection
    @section('sub-title')
        biodata
    @endsection
    @section('content')
        
    
    <h1>Halaman Biodata</h1>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim"method="post">
        @csrf
    <label>First Name:
    </label><br>
    <input type="text" name="fname" id=""><br><br>
    <label>Last Name:
    </label><br>
    <input type="text" name="lname" id=""><br><br>
    <label>Gender:</label><br>
    <input type="radio">Male <br>
    <input type="radio">Female <br>
    <input type="radio">Other <br>
    <label>Nationality:
    </label><br><br>
    <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
    </select><br><br>
    <label>Language Spoken:
    </label><br><br>
    <input type="checkbox" name="language">Bahasa Indonesia <br>
    <input type="checkbox" name="language">English <br>
    <input type="checkbox" name="language">Other  <br>
    <label>Bio :
    </label><br>
    <textarea name="" id="" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="kirim">
</form>
@endsection
</body>
</html>