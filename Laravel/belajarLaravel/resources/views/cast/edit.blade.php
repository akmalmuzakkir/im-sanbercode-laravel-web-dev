@extends('layouts.master')
@section('title')
Halaman edit cast
    
@endsection
@section('sub-title')
    cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>cast nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
        
    @enderror
    <div class="form-group">
      <label >umur</label>
      <textarea name="umur" class="form-control" cols="30" rows="10">{{$cast->umur}}</textarea>
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
        
    @enderror
    <div class="form-group">
      <label >bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
        
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection