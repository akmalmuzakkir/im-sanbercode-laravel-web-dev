<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>contoh looping</h1>
    <?php
    echo "<h3>contoh soal 1</h3>";

    echo "<h6>looping 1</h6>";
    $x = 1;
    do{
        echo $x . " - I Love PHP  <br>";
        $x += 2;
    }while($x <= 19);
    echo "<h5>looping 2</h5>";
    $y = 19;
    while ($y >= 1) {
        echo $y . " - I Love PHP  <br>";
        $y -= 2;
    }
    echo "<h3>contoh soal 2</h3>";
    $angka = [23,35,12,26,40];
    echo "array Nomor : ";
    print_r($angka);
    foreach($angka as $value){
        $rest [] = $value%=2;
    }
    echo "<br>";
    echo "Hasil Array modulus 2 : ";
    print_r($rest);
    echo "<h3>contoh soal 3</h3>";
    $trainer = [
        ["rezky","makassar","laravel"],
        ["thio","bandung","python"],
        ["angga","jakarta","reactjs"],
    ];
    foreach($trainer as $val){
        $tampung = [
            'nama' => $val [0],
            'kota' => $val [1],
            'materi' => $val [2],
        ];
        print_r($tampung);
        echo "<br>";
    }
    echo "<h3>contoh soal 4</h3>";
    for($j=1; $j<=5; $j++){
        for($r=$j;$r <= 5; $r++){
            echo " * ";
        }
        echo "<br>";
    }
    ?>


    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        echo "<h5>LOOPING PERTAMA</h5>";
            $x1 = 2;
    do{
        echo $x1 . " - I Love PHP <br>";
        $x1 += 2;
    }while($x1 <= 20);
     echo "<h5>LOOPING KEDUA</h5>";
    $y1 = 20;
    while ($y1 >= 2) {
        echo $y1 . " - I Love PHP  <br>";
        $y1 -= 2;
    }

        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 

            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini


        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini
                    foreach($numbers as $value){
        $resti [] = $value%=5;
    }
    print_r($resti);
        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        echo "<br>";
            foreach($numbers as $value){
        $resti [] = $value%=5;
    }
    print_r($resti);

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 

            Jangan ubah variabel $items

        */
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
            foreach($items as $val1){
        $tampung1 = [
            'id' => $val1 [0],
            'name' => $val1 [1],
            'price' => $val1 [2],
            'description' => $val1 [3],
            'source' => $val1 [4],
        ];
        print_r($tampung1);
        echo "<br>";
    }
        // Output: 
        
        echo "<h3>Soal No 4 Asterix </h3>";
        /* 
            Soal No 4
            Asterix 5x5
            Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: 
            Output: 
            * 
            * * 
            * * * 
            * * * * 
            * * * * *
        */
        echo "Asterix: ";
        echo "<br>";
        for($j=1; $j<=5; $j++){
            for($r=$j;$r <= 5; $r++){
                echo " * ";
            }
            echo "<br>"; 
 
    }      
    ?>
</body>
</html>